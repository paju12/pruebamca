package com.carlospajuelo.pruebamca.services.impl;

import com.carlospajuelo.pruebamca.dtos.ProductDetailDto;
import com.carlospajuelo.pruebamca.exceptions.NotFoundException;
import com.carlospajuelo.pruebamca.services.SimilarService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;

@Service
public class SimilarServiceImpl implements SimilarService {

    @Value("${similarapi.url}")
    private String url;

    @Cacheable("findSimilars")
    @Override
    public Flux<ProductDetailDto> findSimilars(String id) {
        return WebClient.builder().baseUrl(url).build().get()
                .uri("/product/" + id + "/similarids").retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, clientResponse ->
                        Mono.error(new NotFoundException("Product " + id + " not found")))
                .bodyToFlux(Integer.class)
                .flatMap(this::getProductDetail)
                .sort(Comparator.comparing(ProductDetailDto::getPrice));
    }

    private Mono<ProductDetailDto> getProductDetail(Integer idSimilar) {
        return WebClient.builder().baseUrl(url).build().get()
                .uri("/product/" + idSimilar).retrieve()
                .bodyToMono(ProductDetailDto.class)
                .onErrorResume(WebClientResponseException.class, e -> Mono.empty());
    }

}
