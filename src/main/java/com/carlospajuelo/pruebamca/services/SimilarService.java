package com.carlospajuelo.pruebamca.services;

import com.carlospajuelo.pruebamca.dtos.ProductDetailDto;
import org.springframework.cache.annotation.Cacheable;
import reactor.core.publisher.Flux;

public interface SimilarService {
    @Cacheable("findSimilars")
    Flux<ProductDetailDto> findSimilars(String id);
}
