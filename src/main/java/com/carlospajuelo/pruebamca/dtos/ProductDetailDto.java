package com.carlospajuelo.pruebamca.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "ProductDetail", description = "Product detail")
public class ProductDetailDto {

    @NotBlank
    @Size(min = 1, max = 255)
    private String id;
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
    @NotBlank
    private Float price;
    @NotBlank
    private Boolean availability;

}
