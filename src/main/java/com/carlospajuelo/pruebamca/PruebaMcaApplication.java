package com.carlospajuelo.pruebamca;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
		info = @Info(
				title = "Prueba tecnica",
				version = "1.0.0",
				description = "REST API Documentation",
				contact = @Contact(
						name = "Carlos Pajuelo",
						email = "carlos.pajuelo.fernandez@gmail.com",
						url = "https://carlospajuelo.com/"

				),
				license = @License(
						url = "http://www.apache.org/licenses/LICENSE-2.0.html",
						name = "Apache 2.0"
				)))
@SpringBootApplication
public class PruebaMcaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaMcaApplication.class, args);
	}

}
