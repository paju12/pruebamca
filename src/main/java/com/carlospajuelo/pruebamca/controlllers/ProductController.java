package com.carlospajuelo.pruebamca.controlllers;

import com.carlospajuelo.pruebamca.dtos.ProductDetailDto;
import com.carlospajuelo.pruebamca.exceptions.NotFoundException;
import com.carlospajuelo.pruebamca.services.SimilarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/product")
@Tag(name = "Product")
@Log4j2
public class ProductController {

    @Autowired
    private SimilarService similarService;

    @Operation(operationId = "get-product-similar", summary = "Find similar products",
            description = "You get all the products related to the given id.")
    @GetMapping("/{productId}/similar")
    public Flux<ProductDetailDto> getSimilars(@Parameter(description = "Product ID from which to search for similar products", required = true)
                                                  @PathVariable("productId") String id) throws NotFoundException {
        log.info("Getting similar products for the id " + id);
        return similarService.findSimilars(id);
    }

}
