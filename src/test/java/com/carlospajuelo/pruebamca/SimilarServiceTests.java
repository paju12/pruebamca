package com.carlospajuelo.pruebamca;

import com.carlospajuelo.pruebamca.dtos.ProductDetailDto;
import com.carlospajuelo.pruebamca.exceptions.NotFoundException;
import com.carlospajuelo.pruebamca.services.SimilarService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@SpringBootTest
@Log4j2
class SimilarServiceTests {

    @MockBean
    private SimilarService similarService;

    ProductDetailDto product1 = ProductDetailDto.builder().id("1").name("Shirt").price(9.99f).availability(true).build();
    ProductDetailDto product2 = ProductDetailDto.builder().id("2").name("Dress").price(19.99f).availability(true).build();
    ProductDetailDto product3 = ProductDetailDto.builder().id("3").name("Blazer").price(29.99f).availability(false).build();
    ProductDetailDto product4 = ProductDetailDto.builder().id("4").name("Boots").price(39.99f).availability(true).build();
    ProductDetailDto product100 = ProductDetailDto.builder().id("100").name("Trousers").price(49.99f).availability(false).build();
    ProductDetailDto product1000 = ProductDetailDto.builder().id("1000").name("Coat").price(89.99f).availability(true).build();
    ProductDetailDto product10000 = ProductDetailDto.builder().id("10000").name("Leather jacket").price(89.99f).availability(true).build();

    @Test
    void testGetProduct1() {
        Mockito.when(similarService.findSimilars("1")).thenReturn(Flux.just(product1));

        Flux<ProductDetailDto> response = similarService.findSimilars("1");

        StepVerifier.create(response).expectNext(product1).expectComplete().verify();

        log.info("response: " + response, ProductDetailDto.class);
        response.doOnNext(log::info).subscribe();
    }

    @Test
    void testGetProduct2_4_100() {
        Mockito.when(similarService.findSimilars("2")).thenReturn(Flux.just(product2, product4, product100));

        Flux<ProductDetailDto> response = similarService.findSimilars("2");

        StepVerifier.create(response).expectNext(product2, product4, product100).expectComplete().verify();

        log.info("response: " + response, ProductDetailDto.class);
        response.doOnNext(log::info).subscribe();
    }

    @Test
    void testGetProductNotFound() {
        Mockito.when(similarService.findSimilars("50")).thenReturn(Flux.error(new NotFoundException("Product 50 not found")));

        Flux<ProductDetailDto> response = similarService.findSimilars("50");
        StepVerifier.create(response).expectError(NotFoundException.class).verify();

        log.info("response: " + response);
        response.doOnNext(log::error).subscribe();
    }

}
