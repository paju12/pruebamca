package com.carlospajuelo.pruebamca;

import com.carlospajuelo.pruebamca.controlllers.ProductController;
import com.carlospajuelo.pruebamca.dtos.ProductDetailDto;
import com.carlospajuelo.pruebamca.services.SimilarService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import reactor.core.publisher.Flux;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
@Log4j2
class ControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SimilarService similarService;

    ProductDetailDto product1 = ProductDetailDto.builder().id("1").name("Shirt").price(9.99f).availability(true).build();

    @Test
    void testGetSimilars() throws Exception {
        Mockito.when(similarService.findSimilars("1")).thenReturn(Flux.just(product1));

        String response = mockMvc.perform(get("/product/{productId}/similar", "1"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andReturn().getResponse()
                .getContentAsString();

        log.info("response: " , response, ProductDetailDto.class);
    }

}
