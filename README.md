# Prueba técnica MCA
Esta prueba técnica está realizada basándose en los requisitos propuestos en este ejemplo https://github.com/dalogax/backendDevTest.
Para poder ejecutar la prueba es necesario iniciar los contenedores ubicados en el fichero docker-compose.yml en la ruta `src/main/resources`.

Para poder ejecutar el Docker Compose es necesario crear la imagen Docker del Spring Boot de la prueba, para esto es necesario ir al apartado de **Compilación de la app y creación de la imagen Docker** y una vez creada la imagen volver a este punto.

Para iniciar los servicios es necesario ejecutar el siguiente comando sobre la ruta `src/main/resources`
```
	docker-compose up -d
```
Una vez ejecutando el comando ya estarán disponibles todos los servicios.

## Compilación de la app y creación de la imagen Docker
Para poder compilar la aplicación es necesario estar en el directorio root de la aplicación y ejecutar el siguiente comando.
```
    .\mvnw clean package
```
Una vez ejecutado el anterior comando ya podremos generar la imagen Docker.
Para ello, desde la misma ubicación donde estamos, será necesario ejecutar el siguiente comando
```
    docker build -t carlospajuelo/pruebamca .
```
Cuando termine la ejecución ya se dispondrá de una imagen Docker para ser usada con el nombre **carlospajuelo/pruebamca**

Si se desea ejecutar la imagen solo será necesario ejecutar el siguiente comando
```
    docker run -p 5000:5000 -d carlospajuelo/pruebamca
```

## Documentación de la API
Para este apartado, la app dispone de una documentación OpenApi 3 para la cual se puede acceder a la interfaz gráfica desde la siguiente URL http://localhost:5000/swagger-ui.html

Al acceder a la siguiente URL se cargará la siguiente web donde se mostraran todos los endpoint disponibles.

<img src="assets/swaggerInicio.PNG" title="Swagger Inicio"/>

Para realizar una llamada al endpoint será tan fácil como desplegar la etiqueta azul del apartado *Product*, pulsar sobre el botón negro que pone "Try it out" e introducir un id en el campo *productId* del apartado *Parameters*.

<img src="assets/swaggerTry.PNG" title="Swagger ejecución"/>
Cuando este todo listo solo será necesario pulsar sobre el botón azul con el texto "Execute" y se lanzará la petición, mostrándose el resultado en el apartado de *Responses* donde se podrá ver tanto el código de respuesta como el contenido.

<img src="assets/test4.PNG" title="test4"/>

Sí se desea obtener el fichero json con la definición se tendrá que acceder a la siguiente URL http://localhost:5000/v3/api-docs.

## Testing
La app cuenta con test unitarios basados en Junit, los cuales están ubicados en `src/test/java`

Mediante el swagger que viene incluido se han probado los siguientes casos de uso

    - idProducto: 1
    - idProducto: 2
    - idProducto: 3
    - idProducto: 4
    - idProducto: 5
    - idProducto: 6
    - idProducto: 7
    - idProducto: 8

Se adjuntan las capturas de algunas de las ejecuciones, en las que se pueden ver los 3 casos diferentes de respuesta que tiene actualmente la app.

<img src="assets/test1.PNG" title="test1"/>
<img src="assets/test4.PNG" title="test4"/>
<img src="assets/test6.PNG" title="test6"/>
