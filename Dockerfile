FROM openjdk:17-jdk-alpine

LABEL maintainer="carlos.pajuelo.fernandez@gmail.com"

ENV TZ Europe/Madrid
ENV LANGUAGE es_ES

EXPOSE 5000

ADD target/pruebamca*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]